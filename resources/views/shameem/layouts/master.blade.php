<!DOCTYPE HTML>
<html>
<head>
	@include('shameem.partial.head')
</head>
<body>
		@include('shameem.partial.header')

			@include('shameem.partial.footer')

			@yield('content')
				@include('shameem.partial.nav')
				@include('shameem.partial.script')
			</body>
			</html>

