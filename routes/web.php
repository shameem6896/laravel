<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('shameem/index');
});


Route::get('/about', function () {
    return view('shameem/about');
});


Route::get('/services', function () {
    return view('shameem/services');
});


Route::get('/contact', function () {
    return view('shameem/contact');
});
